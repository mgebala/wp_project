import Unsplash, { toJson } from 'unsplash-js'
import { createSelector } from 'reselect'

import { ORDER_BY_LATEST } from '../constants'

const unsplash = new Unsplash({ accessKey: '59a5758d6b2d9981a0f52dc4a86e5afeadcaba5beb94605f62ec6cfc71c69823' })

const FETCH_US_SECTION_PENDING = 'FETCH_US_SECTION_PENDING'
const FETCH_US_SECTION_FULFILLED = 'FETCH_US_SECTION_FULFILLED'
const FETCH_US_SECTION_REJECTED = 'FETCH_US_SECTION_REJECTED'
const CHANGE_SECTION_ORDER = 'CHANGE_SECTION_ORDER'

const initialState = {
  sectionsNames: {
    404339: 'Świat roślin',
    1227291: 'Świat dyni',
    1103088: 'Jeden kolor',
    4961056: 'Ludzie, doświadczenie, energia',
    3323575: 'Dragi'
  },
  photosPerSection: 25,
  sectionsOrderBy: ORDER_BY_LATEST,
  sectionsLatest: {},
  sectionsPopular: {},
  sectionsFetch: {},
  sectionsError: {}
}

export const getSectionsLatestLimit10 = createSelector(
  state => state.sectionsLatest,
  sections => {
    const returnObj = {}
    Object.keys(sections).forEach(id => {
      returnObj[id] = sections[id].filter((section, index) => index < 10)
    })
    return returnObj
  }
)

export const getSortedSection = createSelector(
  [state => state.sectionsLatest,
    state => state.sectionsPopular,
    state => state.sectionsOrderBy],
  (sectionsLatest, sectionsPopular, sectionsOrderBy) => (
    sectionsOrderBy === ORDER_BY_LATEST ? sectionsLatest : sectionsPopular
  )
)

export const unsplashPhoto = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_SECTION_ORDER:
      return {
        ...state,
        sectionsOrderBy: action.payload
      }
    case FETCH_US_SECTION_PENDING:
      return {
        ...state,
        sectionsFetch: {
          ...state.sectionsFetch,
          [action.payload.id]: true
        }
      }

    case FETCH_US_SECTION_REJECTED:
      return {
        ...state,
        sectionsFetch: {
          ...state.sectionsFetch,
          [action.payload.id]: action.payload.error
        }
      }

    case FETCH_US_SECTION_FULFILLED: {
      const { orderBy, id, photos } = action.payload
      const sectionOrderName = orderBy === ORDER_BY_LATEST ? 'sectionsLatest' : 'sectionsPopular'

      return {
        ...state,
        [sectionOrderName]: {
          ...state[sectionOrderName],
          [id]: [
            ...(state[sectionOrderName][id] || []),
            ...photos
          ]
        },
        sectionsFetch: {
          ...state.sectionsFetch,
          [action.payload.id]: false
        }
      }
    }

    default:
      return state
  }
}

export const changeSectionOrder = orderBy => dispatch => dispatch({
  type: CHANGE_SECTION_ORDER,
  payload: orderBy
})


export const fetchUnsplashSection = ({
  pages = 0,
  photosOnPage = 50,
  order = ORDER_BY_LATEST,
  collectionsId
}) => async dispatch => {
  for (let i = 0, iMax = collectionsId.length; i < iMax; i++) {
    dispatch({
      type: FETCH_US_SECTION_PENDING,
      payload: {
        id: collectionsId[i],
        orderBy: order
      }
    })

    try {
      const payload = await unsplash.collections.getCollectionPhotos(collectionsId[i], pages, photosOnPage, order)
        .then(toJson)
        .then(results => ({
          id: collectionsId[i],
          orderBy: order,
          photos: results.map(section => ({
            description: section.description,
            alt_description: section.alt_description,
            likes: section.likes,
            link: section.links.html,
            urls: {
              thumb: section.urls.thumb,
              full: section.urls.full
            }
          }))
        }))
      dispatch({ type: FETCH_US_SECTION_FULFILLED, payload })
    } catch (err) {
      dispatch({ type: FETCH_US_SECTION_REJECTED, payload: err })
    }
  }
}
