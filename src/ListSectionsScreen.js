import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { ORDER_BY_LATEST } from './constants'
import { fetchUnsplashSection, getSectionsLatestLimit10 } from './stores/unsplashPhotos'
import SectionWrapper from './components/SectionWrapper'

export default
@connect(({
  unsplashPhoto,
  unsplashPhoto: { sectionsNames, photosPerSection }
}) => ({
  latestSectionsWithLimit: getSectionsLatestLimit10(unsplashPhoto),
  sectionsNames,
  photosPerSection
}))
class ListSectionsScreen extends Component {
  constructor() {
    super()
    this.sectionOnClickFn = this.sectionOnClickFn.bind(this)
  }

  static propTypes = {
    latestSectionsWithLimit: PropTypes.object,
    sectionsNames: PropTypes.object,
    photosPerSection: PropTypes.number,
    dispatch: PropTypes.func,
    history: PropTypes.object
  }

  componentDidMount() {
    const {
      sectionsNames,
      photosPerSection,
      dispatch,
      latestSectionsWithLimit
    } = this.props
    const sectionsNamesKeys = Object.keys(sectionsNames)
    const shouldDownloadData = Object.keys(latestSectionsWithLimit).length !== sectionsNamesKeys.length
    if (shouldDownloadData) {
      dispatch(fetchUnsplashSection({
        collectionsId: sectionsNamesKeys,
        order: ORDER_BY_LATEST,
        pages: 1,
        photosOnPage: photosPerSection
      }))
    }
  }

  sectionOnClickFn(sectionId) {
    const { history } = this.props
    window.scrollTo(0, 0)
    history.push(`/section/${sectionId}`)
  }

  render() {
    const { latestSectionsWithLimit, sectionsNames } = this.props
    return (
      <div>
        {latestSectionsWithLimit && <SectionWrapper
          sections={latestSectionsWithLimit || {}}
          sectionsNames={sectionsNames || {}}
          sectionOnClickFn={this.sectionOnClickFn || {}}
        />}
      </div>
    )
  }
}
