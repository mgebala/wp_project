import React from 'react'
import { shallow } from 'enzyme'
import ToogleButton from './ToogleButton'

describe('ToogleButton', () => {
  const exampleData = ['op1', 'op2']
  test('Contain correct text options', () => {
    const wprapper = shallow(<ToogleButton options={exampleData} selectFirst={true} changeValueFn={() => {}} />)
    expect(wprapper.text()).toContain(exampleData.toString().replace(',', ''))
  })
})
