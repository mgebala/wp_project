import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const ToogleButton = props => {
  const { options, selectFirst, changeValueFn } = props
  return (
    <ToogleButtonStyled selectFirst={selectFirst}>
      <span onClick={() => !selectFirst && changeValueFn()}>{options[0]}</span>
      <div onClick={changeValueFn}></div>
      <span onClick={() => selectFirst && changeValueFn()}>{options[1]}</span>
    </ToogleButtonStyled>
  )
}

const ToogleButtonStyled = styled.div`
  display: inline-block;
  padding: 5px 10px;
  margin: 5px auto;
  line-height: 30px;
  cursor: pointer;
  user-select: none;

  span, div {
    color: #333;
    display: inline-block;
  }

  span{
    font-family: arial;
    cursor: pointer;

    &:nth-child(1){
      text-decoration: ${({ selectFirst }) => (selectFirst ? 'underline' : 'none')};
    }

    &:nth-child(3){
      text-decoration: ${({ selectFirst }) => (!selectFirst ? 'underline' : 'none')};
    }
    &:hover{
      text-decoration: underline;
    }
  }

  div {
    width: 50px;
    height: 26px;
    border-radius: 15px;
    background: lightgrey;
    position: relative;
    margin: 5px;
    margin-bottom: -10px;
    &:after {
      top: 3px;
      left: 3px;
      transform: ${({ selectFirst }) => (selectFirst ? 'translateX(0px)' : 'translateX(24px)')};
      transition: .3s ease-in-out;
      position: absolute;
      display: block;
      content: '';
      background: #fff;
      width: 20px;
      height: 20px;
      border-radius: 15px;

    }
  }
  border-bottom: 1px solid lightgrey;
  border-radius: 10px;
`

ToogleButton.propTypes = {
  options: PropTypes.array,
  selectFirst: PropTypes.bool,
  changeValueFn: PropTypes.func
}

export default ToogleButton
