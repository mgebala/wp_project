import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import PreloaderGif from '../assets/preloader.gif'

const SectionWrapper = ({
  sections,
  sectionsNames,
  sectionOnClickFn,
  photoOnClickFn,
  sectionsFetch,
  sectionId
}) => {
  const PreloaderStyled = styled.img`
    margin: 50px auto;
    display: block;
  `

  const SectionWrapperStyled = styled.div`
    ${props => props.clickableSection && `
      ul {
        border-radius: 20px;
        border-left: 3px solid grey;
        transition: border-color 0.5s ease;
        cursor:pointer;

        &:hover {
          border-left: 3px solid red;
        }
      }
    `}

    margin-bottom: 50px;

    h2 {
      width: 80%
      margin: 10px auto;
      margin-top: 50px;
      text-align: center;
      font-family: arial;
      cursor: pointer;
    }

    ul {
      column-cout: 2;
      width: 80%;
      max-width: 1200px;
      height: auto;
      text-align: center;
      display: flex;
      flex-wrap: wrap;
      justify-content: left;
      margin: 10px auto;
      padding: 0px;
      text-align: left;

      li {
        vertical-align: top;
        list-style-type: none;
        display: inline-block;
        position: relative;
        &:hover{
          span{
            opacity: ${props => (props.clickableSection ? 0 : 1)};
          }
        }
        span {
          display: block;
          position: absolute;
          color: #fff;
          top: 15px;
          left: 15px;
          background: rgba(0,0,0,0.3);
          border-radius: 5px;
          padding: 2px 5px;
          opacity: 0;
          transition: opacity 0.2s ease-in-out;
        }
        img {
          margin: 10px;
          border-radius: 10px;
          display: block;
          cursor: pointer;
          object-fit: cover
          width: 220px;
          height: 160px;
        }

      }
    }
  `
  return (
    <section>
      {sections && Object.keys(sections).map((key, keyIndex) => <SectionWrapperStyled
        key={`${keyIndex}-${key}`}
        clickableSection={sectionOnClickFn}
        onClick={() => sectionOnClickFn && sectionOnClickFn(key)}>
        {sectionsNames && <h2>{sectionsNames[key]}</h2>}
        <ul>
          {sections[key] && sections[key].map((photo, index) => <li
            key={`${index}-${photo.urls.thumb}`}
            onClick={() => photoOnClickFn && photoOnClickFn(photo)}>
            <span>{photo.likes}</span>
            <img src={photo.urls.thumb} />
          </li>
          )}
        </ul>
      </SectionWrapperStyled>
      )}
      {sectionsFetch && sectionId && sectionsFetch[sectionId] && <PreloaderStyled src={PreloaderGif} alt="preloader" />}
    </section>
  )
}

SectionWrapper.propTypes = {
  clickableSection: PropTypes.bool,
  sections: PropTypes.object,
  sectionsNames: PropTypes.object,
  sectionOnClickFn: PropTypes.func,
  photoOnClickFn: PropTypes.func,
  sectionsFetch: PropTypes.object,
  sectionId: PropTypes.string
}

export default SectionWrapper
