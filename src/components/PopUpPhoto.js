import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

export default class PopUpPhoto extends Component {
  constructor() {
    super()
    this.state = {
      imgLoadingCompleted: false
    }
  }

  static propTypes = {
    src: PropTypes.string,
    likes: PropTypes.number,
    link: PropTypes.string,
    description: PropTypes.string
  }

  componentDidMount() {
    if (window.FB && window.FB.XFBML) {
      window.FB.XFBML.parse()
    }

    const img = new Image()
    img.src = this.props.src
    img.onload = () => this.setState({ imgLoadingCompleted: true })
  }

  render() {
    const { imgLoadingCompleted } = this.state
    const {
      description,
      link,
      likes,
      src
    } = this.props
    return (<PopUpPhotoStyled>
      <span>{description}</span>

      <PhotoStyled imgLoadingCompleted={imgLoadingCompleted}>
        <span>{likes}</span>
        <img src={src} alt={description} />
      </PhotoStyled>

      <div className="fb-share-button" data-href={link} data-layout="button_count"></div>
      <div className="fb-like" data-href={link} data-layout="standard" data-action="like" data-show-faces="true"></div>
    </PopUpPhotoStyled>)
  }
}

const PhotoStyled = styled.div`
  position: relative;
  opacity: ${props => (props.imgLoadingCompleted ? 1 : 0)};
  transition: opacity 1s ease-in-out;

  & > img {
    display:block;
    max-width: 600px;
    max-height: 600px;
    margin: 10px auto;
    border-radius: 10px;
    display: block;
  }

  span {
    display:block;
    position:absolute;
    color: #fff;
    top: 5px;
    left: 5px;
    background: rgba(0,0,0,0.3);
    border-radius: 5px;
    padding: 2px 5px;
  }
`

const PopUpPhotoStyled = styled.div`
  & > span {
    display: block;
    text-align: justify;
    &::first-letter {
      text-transform: uppercase;
    }
  }

  .fb-like, .fb-share-button{
    display: block;
    float:left;
    margin-right: 5px;
  }

  .fb-like{
    width: 260px;
  }
`
