import React from 'react'
import { shallow } from 'enzyme'
import SectionWrapper from './SectionWrapper'

describe('SectionWrapper', () => {
  const exampleSections = {
    404339: [{
      alt_description: 'example description',
      description: null,
      likes: 140,
      link: 'https://unsplash.com/photos/Lv5ndK10jZo',
      urls: {
        full: 'https://unsplash.com/photos/full',
        thumb: 'https://unsplash.com/photos/thimb'
      }
    }]
  }

  const examplesectionsNames = {
    404339: 'my first section'
  }

  let wrapper
  beforeEach(() => {
    wrapper = shallow(<SectionWrapper sectionsNames={examplesectionsNames} sections={exampleSections} />)
  })

  it('Contain correct h2 (title)', () => {
    expect(wrapper.find('h2').text()).toContain(examplesectionsNames[404339])
  })

  it('Contain correct count likes', () => {
    expect(wrapper.find('span').text()).toContain(exampleSections[404339][0].likes)
  })
})
