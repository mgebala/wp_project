export const ORDER_BY_POPULAR = 'popular'
export const ORDER_BY_LATEST = 'latest'
export const LATEST = 'najnowsze'
export const POPULAR = 'popularne'
export const TIME_ROUTE_ANIMATION_MS = 500
