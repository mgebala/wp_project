import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import {
  createStore,
  compose,
  applyMiddleware,
  combineReducers
} from 'redux'
// import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import styled from 'styled-components'

import ListSectionScreen from './ListSectionsScreen'
import SectionScreen from './SectionScreen'

import { unsplashPhoto } from './stores/unsplashPhotos'
import { TIME_ROUTE_ANIMATION_MS } from './constants'

const composeEnhancer = process.env.NODE_ENV !== 'production' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose : compose
const middleware = [promise, thunk]
// if (process.env.NODE_ENV !== 'production') middleware.push(createLogger())
const store = createStore(
  combineReducers({
    unsplashPhoto
  }),
  composeEnhancer(
    applyMiddleware(...middleware)
  )
)

const TransitionGroupStyled = styled(TransitionGroup)`
  .routerView-enter {
    opacity: 0;
    transform: translateX(100%);
  }
  .routerView-enter-active {
    opacity: 1;
    transition: transform ${TIME_ROUTE_ANIMATION_MS}ms;
    transform: translateX(0%);
  }
  .routerView-exit {
    transform: translateX(0%);
  }
  .routerView-exit-active {
    transform: translateX(-100%);
    transition: transform ${TIME_ROUTE_ANIMATION_MS}ms;
  }
`

render(
  <Provider store={store}>
    <BrowserRouter>
      <Route render={({ location, location: { key } }) => <TransitionGroupStyled>
        <CSSTransition key={key} classNames='routerView' timeout={TIME_ROUTE_ANIMATION_MS}>
          <Switch location={location}>
            <Route exact path="/" component={ListSectionScreen} />
            <Route exact path="/section/:id" component={SectionScreen} />
          </Switch>
        </CSSTransition>
      </TransitionGroupStyled>
      }/>
    </BrowserRouter>
  </Provider>, document.getElementById('app'))
