import React, { Component } from 'react'

export default WrappedComponent => class extends Component {
  constructor() {
    super()
    this.state = {
      positionTop: 0,
      bottomArea: null,
      visible: null
    }
    this.checkScrollPosition = this.checkScrollPosition.bind(this)
    this.checkScrollVisible = this.checkScrollVisible.bind(this)
  }

  componentDidMount() {
    window.addEventListener('scroll', this.checkScrollPosition)
    window.addEventListener('resize', this.checkScrollVisible)
    this.checkScrollPosition()
    this.checkScrollVisible()
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.checkScrollPosition)
    window.removeEventListener('resize', this.checkScrollVisible)
  }

  checkScrollVisible() {
    const windowHeight = window.innerHeight
    const { clientHeight } = document.body
    this.setState({ visible: (clientHeight > windowHeight) })
  }

  checkScrollPosition() {
    const positionTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0
    const windowHeight = window.innerHeight
    const { clientHeight } = document.body
    const bottomArea = clientHeight <= windowHeight + positionTop
    this.setState({ positionTop, bottomArea })
  }

  render() {
    return <WrappedComponent
      {...this.props}
      scroll={{ ...this.state, checkVisible: this.checkScrollVisible }} />
  }
}
