import React, { Component } from 'react'
import styled from 'styled-components'

import CloseSVG from '../assets/close.svg'

export default WrappedComponent => class extends Component {
  constructor() {
    super()

    this.state = {
      modalVisible: false
    }

    this.component = null
    this.showModal = this.showModal.bind(this)
    this.hideModal = this.hideModal.bind(this)
  }

  showModal({ component }) {
    this.component = component
    this.setState({ modalVisible: true })
  }

  hideModal() {
    this.setState({ modalVisible: false })
  }

  render() {
    const { modalVisible } = this.state
    return (
      <div>
        {modalVisible
          && <PopUpStyled>
            <div>
              <CloseIco onClick={this.hideModal}></CloseIco>
              {this.component && this.component}
            </div>
          </PopUpStyled>
        }
        <WrappedComponent modal={{ show: this.showModal, hide: this.hideModal }} {...this.props} />
      </div>
    )
  }
}

const PopUpStyled = styled.div`
  position: fixed;
  z-index: 9999;
  top: 0px;
  left: 0px;
  overflow: hidden;
  width: 100%;
  height: 100%;
  background: rgba(0,0,0,0.8);
  & > div {
    padding: 10px;
    background: #fff;
    max-width: 600px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 10px;
  }
`
const CloseIco = styled.div`
  z-index: 999;
  position: absolute;
  top: -15px;
  right: -15px;
  width: 30px;
  height: 30px;
  font-size: 40px;
  line-height: 30px;
  text-align: center;
  border-radius: 20px;
  cursor: pointer;
  transition-duration: 1s;
  background: url(${CloseSVG}) #fff center no-repeat;
  &:hover {
    transform: rotate(180deg);
  }
`
