import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import scrollHelper from './hoc/scrollHelper'
import modalHelper from './hoc/modalHelper'
import backArrow from './assets/backArrow.png'
import {
  fetchUnsplashSection,
  changeSectionOrder,
  getSortedSection
} from './stores/unsplashPhotos'

import SectionWrapper from './components/SectionWrapper'
import ToogleButton from './components/ToogleButton'
import PopUpPhoto from './components/PopUpPhoto'

import {
  ORDER_BY_POPULAR,
  ORDER_BY_LATEST,
  LATEST,
  POPULAR
} from './constants'

export default
@scrollHelper
@modalHelper
@connect(({
  unsplashPhoto, unsplashPhoto: {
    sectionsLatest,
    sectionsPopular,
    sectionsNames,
    photosPerSection,
    sectionsOrderBy,
    sectionsFetch
  }
}) => ({
  sectionsLatest,
  sectionsPopular,
  sectionsNames,
  photosPerSection,
  sectionsOrderBy,
  sectionsFetch,
  sortedSection: getSortedSection(unsplashPhoto)
}))
class ListSectionsScreen extends Component {
  constructor() {
    super()
    this.state = {
      sectionId: null,
      sortedByLatest: true
    }

    this.showPopupWithPhoto = this.showPopupWithPhoto.bind(this)
  }

  static propTypes = {
    latestSectionsWithLimit: PropTypes.object,
    sectionsNames: PropTypes.object,
    photosPerSection: PropTypes.number,
    sectionsLatest: PropTypes.object,
    sectionsPopular: PropTypes.object,
    sectionsFetch: PropTypes.object,
    match: PropTypes.object,
    dispatch: PropTypes.func,
    sectionsOrderBy: PropTypes.string,
    scroll: PropTypes.object,
    sortedSection: PropTypes.object,
    modal: PropTypes.object
  }

  componentDidMount() {
    const {
      photosPerSection,
      sectionsLatest,
      match: { params },
      dispatch
    } = this.props

    const sectionId = params.id
    this.setState({ sectionId })

    if (sectionId && !sectionsLatest[sectionId]) {
      dispatch(fetchUnsplashSection({
        collectionsId: [sectionId],
        order: ORDER_BY_LATEST,
        pages: 1,
        photosOnPage: photosPerSection
      }))
    }
  }

  componentDidUpdate(prevProps) {
    const {
      photosPerSection,
      sectionsOrderBy,
      dispatch,
      scroll,
      sortedSection,
      sectionsFetch
    } = this.props
    const { sectionId } = this.state
    const lastScroll = prevProps.scroll

    if (!sectionsFetch[sectionId] && prevProps !== this.props) {
      if (!sortedSection[sectionId]
        || (scroll.bottomArea && scroll.bottomArea !== lastScroll.bottomArea && lastScroll.bottomArea !== null)
        || sortedSection[sectionId].length < 50) {
        const countPhoto = sortedSection[sectionId] ? sortedSection[sectionId].length : 0
        dispatch(fetchUnsplashSection({
          collectionsId: [sectionId],
          pages: (countPhoto - (countPhoto % photosPerSection)) / photosPerSection + 1,
          photosOnPage: photosPerSection,
          order: sectionsOrderBy
        }))
      }
    }
  }

  showPopupWithPhoto({
    link,
    description,
    alt_description,
    likes,
    urls
  }) {
    const { modal } = this.props
    const openPopup = {
      show: true,
      link,
      description: description || alt_description,
      likes,
      src: urls.full
    }

    modal.show({
      component: <PopUpPhoto {...openPopup} />
    })
  }

  render() {
    const {
      sectionsNames,
      sectionsOrderBy,
      dispatch,
      sortedSection,
      sectionsFetch
    } = this.props
    const { sectionId } = this.state
    return (
      <SectionScreenStyled>
        <nav>
          <BackArrowIco to="/" />
          <ToogleButton
            selectFirst={sectionsOrderBy === ORDER_BY_LATEST}
            changeValueFn={() => dispatch(
              changeSectionOrder(sectionsOrderBy === ORDER_BY_LATEST ? ORDER_BY_POPULAR : ORDER_BY_LATEST)
            )}
            options={[LATEST, POPULAR]} />
        </nav>

        {sectionId && <SectionWrapper
          sections={{ sectionId: sortedSection[sectionId] }}
          sectionId={sectionId}
          sectionsFetch={sectionsFetch}
          sectionsNames={sectionsNames}
          photoOnClickFn={this.showPopupWithPhoto}/>}
      </SectionScreenStyled>
    )
  }
}

const SectionScreenStyled = styled.div`
  nav{
    text-align:center;
    display: block;
    width: 80%;
    position: relative;
    margin: 0px auto;
    a{
      display:block;
      position: absolute;
      left: 10%;
      @media (max-width: 600px) {
        display:block;
        left: 50%;
        transform: translateX(-50%);
        position: relative;
      }
    }
  }
`

const BackArrowIco = styled(Link)`
  width: 50px;
  height: 50px;
  display: inline-block;
  background-image: url(${backArrow});
  background-size: 50px 50px;
  border: 0px;
  outline: 0px;
  user-select: none;
  transition: opacity .5s ease-in-out;
  opacity: .5;
  &:hover{
    opacity: .8;
  }
`
