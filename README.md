## [2019] Projekt WPUnsplash - Marcin Gębala

### Szybki Start:

Po pobraniu repozytorium instalujemy wszystkie zależności:

```
npm
```

oraz odpalamy projekt w trybie deweloperskim:

```
npm start
```

budowanie projektu w trybie produkcyjnym:

```
npm start
```

wywołanie testów:

```
npm test
```

### Założenia:

* stack technologiczny: *react, redux, webpack, babel, styledComponents, eslint, jest*
* do cache'owania getterów/selektorów reduxa wykorzystuje **reselect**
* z uwagi na wielkość projektu akcje i reducery umieściłem w jednym pliku store'a: **unsplashPhotos**
* aby uzyskać płaski redux'owy *state* zamiast podkluczy mamy oddzielne węzły
* pokusiłem sie o napisanie własnego *infinite sroll* który jest oddzielnym **Higher-Order Components** z możliwością dołączenia do wybranego komponentu za pomocą *dekoratora* choć zazwyczaj dobrze jest użyć już gotowej i popularnej biblioteki
* nie korzystam z **mapDispatchToProps** kreatory akcji importuje bezpośrednio z pliku inaczej w większych projektach trudno odróżnić co jest akcją a co zwykłym propsem
* wyodrębniłem plik na stałe, token do unsplash zostawiłem choć pewnie dobrze byłoby go w miarę możliwośći przenieść i wywoływać po stronie node'a tak aby był niedostępny dla zwykłego użytkownika
* aplikacja posiada 3 widoki, dwa z nich są realizowane za pomocą standardowych widoków reaktowych z dodaną animacją przejścia, trzeci jest popup'em

### Podgląd:

![alt text](https://chomiczek.uploadfile.pl/pobierz/1849300---lvh4/5316677400_1326980696.jpg)
