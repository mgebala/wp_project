const HtmlWebPackPlugin = require('html-webpack-plugin')

module.exports = (env, argv) => ({
  output: {
    filename: 'main-[hash].js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        loader: 'file-loader',
        options: {
          outputPath: argv.mode === 'development' ? '' : '/assets',
          publicPath: argv.mode === 'development' ? '/' : '/assets'
        }
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: './index.html'
    })
  ]
})
